package co.simplon.nuxyphere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NuxyphereApplication {

	public static void main(String[] args) {
		SpringApplication.run(NuxyphereApplication.class, args);
	}

}
